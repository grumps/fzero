A simple static site builder
===============================

version number: 0.0.1
author: Max Resnick

Overview
--------

A simple static site builder

Installation / Usage
--------------------

To install use pip:

    $ pip install fzero


Or clone the repo:

    $ git clone https://github.com/grumps/fzero.git
    $ python setup.py install
    
Contributing
------------

TBD

Example
-------

```
fz build
```
