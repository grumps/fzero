# -*- coding: utf-8 -*-
import os
import shutil
import glob
import piexif
import json

from piexif import ImageIFD, helper


def _d(desc):
    """scrub user comments field"""
    prefix = '\x00\x00\x00\x00\x00\x00\x00\x00'
    try:
        return helper.UserComment.load(desc)
    except ValueError:
        return desc.decode('utf-8').replace(prefix, '')


def find_images(path):
    return glob.glob(f'{path}/**/*.jpg', recursive=True)


def build_image_data(images):
    unordered = []
    for image in images:
        image_meta = piexif.load(image)
        unordered.append(
            (image,
             _d(image_meta['0th'].get(ImageIFD.ImageDescription)),
             image_meta['0th'].get(ImageIFD.DateTime, '').decode())
        )
    return sorted(unordered, key=lambda i: i[2], reverse=True)


def compile_images(conf):
    path = conf['paths']['image_path']
    return build_image_data(find_images(path))


def render_images(conf, images_data):
    url_base = os.path.join(conf['paths']['base_url'],
                            conf['paths']['media_slug'])
    render_ready = []
    for path, desc, created in images_data:
        render_ready.append({'url': os.path.join(url_base, path),
                             'desc': desc,
                             'created': created})
        copy_path = os.path.join(conf['paths']['project_root'],
                                 conf['paths']['build_dest'],
                                 conf['paths']['media_slug'],
                                 path)
        try:
            shutil.copy2(path, copy_path)
        except NotADirectoryError:
            os.makedirs(os.path.dirname(copy_path))

    # TODO handling walking
    with open(f'{conf["build_dest"]}/images.json', 'w') as f:
        return json.dump({'objects': render_ready, 'next': 'END'}, f)
