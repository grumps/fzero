# -*- coding: utf-8 -*-
import os
import sys
import logging
import glob
import http.server

import click
from ruamel.yaml import YAML
from fzero import builders
from fzero import images

log = logging.getLogger('fzero')
formatter = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
handler = logging.StreamHandler(stream=sys.stdout)
handler.setFormatter(formatter)
handler.setLevel(logging.DEBUG)
log.addHandler(handler)
log.setLevel(logging.DEBUG)

yaml = YAML(typ='safe')

_defaults = {'project_root': os.getcwd()}


def config():
    cwd = os.getcwd()
    conf = dict(**_defaults)
    default_path = os.path.join(os.path.dirname(__file__), 'fzero.yml')
    confs = glob.glob(f'{cwd}/*fzero.yml')
    confs.append(default_path)
    with open(confs[0], 'r') as f:
        conf.update(yaml.load(f))
        return conf

# TODO refactor to CLI

@click.group()
@click.pass_context
def cli(ctx):
    ctx.obj = config()
    click.echo('...running fzero...')


@cli.command()
@click.argument('path')
@click.pass_context
def iz(ctx, path):
    # need to be able to selectively build
    image_ctx = images.compile_images(ctx.obj)
    images.render_images(ctx.obj, image_ctx)


@cli.command()
@click.pass_context
def sv(ctx):
    os.chdir(ctx.obj['build_dest'])
    server_address = ('', 8000)
    httpd = http.server.HTTPServer(server_address,
                                   http.server.SimpleHTTPRequestHandler)

    httpd.serve_forever()

@cli.command()
@click.pass_context
def build(ctx):
    """full build"""
    builders.build(ctx.obj)


