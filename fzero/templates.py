import glob
import os
import shutil

from ruamel.yaml import YAML
from jinja2 import Environment, FileSystemLoader, select_autoescape

yaml = YAML(typ='safe')


def compile_content(conf):
    # slurp content files
    conf['content'] = {}
    content_path = os.path.join(os.getcwd(), conf['paths']['content'])
    for path in glob.glob(f'{content_path}/**/*.yml', recursive=True):
        with open(path, 'r') as c:
            content = yaml.load(c)
            conf['content'][content['slug']] = content


def compile_templates(conf):
    return Environment(
        loader=FileSystemLoader(conf['paths']['templates_path']),
        autoescape=select_autoescape(['html'])
    )


def render_templates(conf, templates):
    for slug, content in conf['content'].items():
        template = templates.get_template(content['template'])
        basename, name = os.path.split(content['url'])
        abs_path = os.path.join(conf['paths']['build_dest'], basename)
        try:
            os.makedirs(abs_path)
        except FileExistsError:
            shutil.rmtree(abs_path)
            os.makedirs(abs_path)

        with open(f'{abs_path}/{name}.html', 'w') as f:
            f.write(template.render(**content))
