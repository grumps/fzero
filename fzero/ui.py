import os

import sass


def compile_sass(conf):
    try:
        include_path = os.path.join(f'{conf["project_root"]}',
                                    f'{conf["node_mod"]}', 'material-components-web')
        return sass.compile(include_paths=[include_path],
                            filename=f'{conf["ui"]}/theme.scss')
    except OSError:
        return ''


def render_css(conf, css):
    with open(f'{conf["build_dest"]}/theme.css', 'w') as f:
        f.write(css)
