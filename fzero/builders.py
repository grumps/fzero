from fzero import images, templates, ui

COMPILERS = (templates.compile_content,
             templates.compile_templates,
             images.compile_images,)
RENDERERS = (None,
             templates.render_templates,
             images.render_images,)


def build(conf, compilers=COMPILERS, renders=RENDERERS):
    for compile_, render in zip(compilers, renders):
        compiled = compile_(conf)
        if compiled:
            render(conf, compiled)
